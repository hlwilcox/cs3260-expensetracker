//
//  ViewController.swift
//  expenseTracker
//
//  Created by Heather Wilcox on 3/11/18.
//  Copyright © 2018 Heather Wilcox. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    var expenses: [Expense] = []
    
    @IBAction func addExpense(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: "addExpense", sender: self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        //Request to get everything from the table.
        let fetchDatabase = NSFetchRequest<NSManagedObject>(entityName: "Expense")
        
        do {
            //popluating the array
            expenses = try managedContext.fetch(fetchDatabase) as! [Expense]
            tableView.reloadData()
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        //if Bundle.main.path(forResource: "Expense", ofType: "") != nil
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return expenses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        if (cell == nil) {
            cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        }
    
        let object = expenses[indexPath.row]
        cell?.textLabel?.text = (object.locationName)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.performSegue(withIdentifier: "editExpense", sender: expenses[indexPath.row])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "editExpense"){
            let detailsVC = segue.destination as! ExpenseDetailsViewController
            detailsVC.expense = sender as? Expense
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

