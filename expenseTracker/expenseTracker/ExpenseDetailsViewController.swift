//
//  ExpenseDetailsViewController.swift
//  expenseTracker
//
//  Created by Heather Wilcox on 3/11/18.
//  Copyright © 2018 Heather Wilcox. All rights reserved.
//

import UIKit
import CoreData

class ExpenseDetailsViewController: UIViewController {
    //Array to hold the object from the database.
    var expense: Expense? = nil
    
    @IBOutlet weak var locationTxtFld: UITextField!
    @IBOutlet weak var dateTxtFld: UITextField!
    @IBOutlet weak var notesTxtVw: UITextView!
    @IBOutlet weak var amountTxtFld: UITextField!
    @IBOutlet weak var typeTxtFld: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // Recieve object from the array of what's in the database.
        // Display it.
        if (expense != nil) {
            //put data in the textfields
            
            self.locationTxtFld.text = expense?.locationName
            self.dateTxtFld.text = expense?.date
            self.notesTxtVw.text = expense?.notes
            self.amountTxtFld.text = expense?.cost
            self.typeTxtFld.text = expense?.type
        } else {
            self.locationTxtFld.text = "Location"
            self.dateTxtFld.text = "Date"
            self.notesTxtVw.text = "Notes"
            self.amountTxtFld.text = "Amount"
            self.typeTxtFld.text = "Type"
        }
        
        let keyboard = UIGestureRecognizer(target: self, action: #selector(removeKeyboard))
        self.view.addGestureRecognizer(keyboard)
    }
    
    @objc func removeKeyboard(){
        self.view.endEditing(true)
    }
    
    
    // When the Save button is pressed.
    // We need the data that was entered in the fields to be saved to the object in the database.
    // Get the object from the array(should correspond with the object in the database.)
    // Edit that objects content.
    // Save it.
    // Have the ViewController refetch the updated table from the database.
    @IBAction func saveChanges(_ sender: Any) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        // Grabbing context?
        let managedContext = appDelegate.persistentContainer.viewContext
        
        
        if (expense != nil) {
            //Changing the values in the first object
            expense?.cost = self.amountTxtFld.text
            expense?.date = self.dateTxtFld.text
            expense?.locationName = self.locationTxtFld.text
            expense?.notes = self.notesTxtVw.text
            
            //Saving the object that has just been changed
            do { try managedContext.save() }
            catch let error as NSError { print("Could not save. \(error), \(error.userInfo)") }
        } else {
            let entity = NSEntityDescription.entity(forEntityName: "Expense", in: managedContext)!
            
            // Creating the object / Row in the db
            let expense = NSManagedObject(entity: entity, insertInto: managedContext)
            
            // Setting values on the object
            expense.setValue("cost", forKeyPath: "cost")
            expense.setValue("date", forKeyPath: "date")
            expense.setValue("locationName", forKeyPath: "locationName")
            expense.setValue("notes", forKeyPath: "notes")
            expense.setValue("type", forKeyPath: "type")
            
            //Saving the object
            do {
                try managedContext.save()
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
/*       // Specifying the table and context to insert into
        let entity = NSEntityDescription.entity(forEntityName: "Expense", in: managedContext)!
        
        // Creating the object / Row in the db
        let person = NSManagedObject(entity: entity, insertInto: managedContext)
        
        // Setting values on the object
        person.setValue("name", forKeyPath: "name")
        person.setValue("some stuff about this person", forKeyPath: "wikipediaEntry")
        person.setValue("some url", forKeyPath: "imageUrl")
        
        //Saving the object
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }

        
        // Creating the request to fetch everything from a specific table
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Expense")
        
        //Editing the content on the table
        do {
            //fetching objects and returning them to an array
            //expense = try managedContext.fetch(fetchRequest) as! [Expense]
            //grabbing the first object; cost, date, locationName, notes
            //let fetchedExpense = expense[0]
            
           
            
            // Fetching the objects that was just changed.
            // Recycling the array, filling it with the new data.
            expense = try managedContext.fetch(fetchRequest) as! [Expense]
            let expenseNewCost = expense[0]
            print(expenseNewCost.cost!)
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
*/
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
